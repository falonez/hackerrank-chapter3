Link https://www.hackerrank.com/challenges/diagonal-difference?isFullScreen=true

'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'diagonalDifference' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts 2D_INTEGER_ARRAY arr as parameter.
 */

function diagonalDifference(arr) {
    // Write your code here
    // console.log(arr);
    // Mendefinisikan variable penampung nilai hasil diagonal 
    let dPirmary=0;
    let dSecondary=0;
    // Mendifinisikan variable penampung pangjang array 
    let jArray=arr.length;
    
    for(let i=0; i<arr.length;i++){
        // console.log(arr[i]);
        // console.log(arr[j])

        // Variable ditambahkan secara diagonal dari kiri ke kanan
        dPirmary+=arr[i][i]; 
        // Variable ditambahkan secara diagonal dari kanan ke kiri 
        dSecondary+=arr[i][jArray-1];
         
        //  console.log(arr[i][jArray-1]);
        // Pengurangan 1 setiap looping
        jArray--; 
    }
        // Proses perhitungan Difference Diagonal
        return dSecondary-dPirmary;
        //  console.log(dSecondary);
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const n = parseInt(readLine().trim(), 10);

    let arr = Array(n);

    for (let i = 0; i < n; i++) {
        arr[i] = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));
    }

    const result = diagonalDifference(arr);

    ws.write(result + '\n');

    ws.end();
}
