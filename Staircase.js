'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function(inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function() {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the 'staircase' function below.
 *
 * The function accepts INTEGER n as parameter.
 */

function staircase(n) {
    // want to declare a variable that will hold the final output that we will print 
    let result = ''
    

    // Looping jumlah baris 
    for (let i = 1; i <= n; i++) {
        
            // Looping untuk me print space dengan jumlah n dikurangi 1
        for (let j = n - 1; j >= i; j--) {
            result += ' '
        }
        
            // Looping untuk print # berdasarkan nilai n sehingga mengisi ruang kosong
        for (let k = 1; k <= i; k++) {
            result += '#'
        }
        // baris baru
        result += "\n"
       
    }
    // Hasil
    console.log(result)
    
    

}

function main() {
    const n = parseInt(readLine().trim(), 10);

    staircase(n);
}
